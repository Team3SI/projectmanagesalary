﻿using System.Collections;
using System.Threading.Tasks;
using SalaryManage.Models;

namespace SalaryManage.Service
{
    public interface IFactoryService
    {
        Factory GetById(string Id);
        IEnumerable GetAll();
        Task Create(Factory  factory);
        void Delete(string Id);
        Task Update(Factory factory);
        void save();
    }
}