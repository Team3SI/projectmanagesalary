﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SalaryManage.UnitOfWork;
using SalaryManage.Models;

namespace SalaryManage.Service
{
    public class FactoryService : IFactoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IFactoryService _factoryServiceImplementation;

        public Factory GetById(string Id)
        {
            return _unitOfWork.factoryRepository.GetById(Id);
        }

        public IEnumerable GetAll()
        {
            return _unitOfWork.factoryRepository.GetAll();
        }

        public Task Create(Factory factory)
        {
            return _unitOfWork.factoryRepository.Create(factory);
        }

        public void Delete(string Id)
        {
            _unitOfWork.factoryRepository.Delete(Id);
        }

        public Task Update(Factory factory)
        {
            return _unitOfWork.factoryRepository.Update(factory);
        }

        public void save()
        {
            _unitOfWork.factoryRepository.save();
        }
    }
}