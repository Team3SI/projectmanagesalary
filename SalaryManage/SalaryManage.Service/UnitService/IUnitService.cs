﻿using System.Collections;
using System.Threading.Tasks;
using SalaryManage.Models;

namespace SalaryManage.Service
{
    public interface IUnitService
    {
        Unit GetById(string Id);
        IEnumerable GetAll();
        Task Create(Unit unit);
        void Delete(string Id);
        Task Update(Unit unit);
        void save();
     
    }
}