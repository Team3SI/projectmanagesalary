﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SalaryManage.UnitOfWork;
using SalaryManage.Models;

namespace SalaryManage.Service
{
    public class UnitService : IUnitService
    {
        private readonly IUnitOfWork _unitOfWork;
        private UnitService _unitServiceImplementation;

        public Unit GetById(string Id)
        {
            return _unitOfWork.unitRepository.GetById(Id);
        }

        public IEnumerable GetAll()
        {
            return _unitOfWork.unitRepository.GetAll();
        }

        public Task Create(Unit unit)
        {
            return _unitOfWork.unitRepository.Create(unit);
        }

        public void Delete(string Id)
        {
            _unitOfWork.unitRepository.Delete(Id);
        }

        public Task Update(Unit unit)
        {
            return _unitOfWork.unitRepository.Update(unit);
        }

        public void save()
        {
            _unitOfWork.unitRepository.save();
        }
    }
}