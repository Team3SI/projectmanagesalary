﻿using Microsoft.Extensions.DependencyInjection;

namespace SalaryManage.Service
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddServiceExtension(this IServiceCollection service)
        {

            service.AddScoped<IPositionService, PositionService>();
            service.AddScoped<IFactoryService, FactoryService>();
            service.AddScoped<IUnitService, UnitService>();
            return service;
        }
    }
}