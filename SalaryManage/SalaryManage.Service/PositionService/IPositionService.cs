﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SalaryManage.Models;

namespace SalaryManage.Service
{
    public interface  IPositionService
    {
        Position GetById(string Id);
        IEnumerable<Position> GetAll();
        Task Create(Position  position);
        void Delete(string Id);
        Task Update(Position position);
        void save();

    }
}