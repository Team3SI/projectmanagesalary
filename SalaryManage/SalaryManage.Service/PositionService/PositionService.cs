﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SalaryManage.UnitOfWork;
using SalaryManage.Models;
namespace SalaryManage.Service
{
    public class PositionService : IPositionService
    {

        private readonly IUnitOfWork _unitOfWork;
        private IPositionService _positionServiceImplementation;

        public PositionService (IUnitOfWork  unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Position GetById(string Id)
        {
            return _unitOfWork.positionRepository.GetById(Id);
        }

        public IEnumerable<Position> GetAll()
        {
            return  _unitOfWork.positionRepository.GetAll();
        }

        public Task Create(Position position)
        {
           return  _unitOfWork.positionRepository.Create(position);
        }

        public void Delete(string Id)
        {
            _unitOfWork.positionRepository.Delete(Id);
        }

        public Task Update(Position position)
        {
            return  _unitOfWork.positionRepository.Update(position);
        }

        public void save()
        {
            _unitOfWork.positionRepository.save();
        }
    }

}