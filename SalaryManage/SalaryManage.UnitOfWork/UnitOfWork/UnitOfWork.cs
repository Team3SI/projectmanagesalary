﻿using SalaryManage.Models;
using SalaryManage.Reponsitory.FactoryRepository;
using SalaryManage.Repository;
namespace SalaryManage.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private SalaryManageContext _context;
        public UnitOfWork(SalaryManageContext context)
        {
            _context = context;
            IniRepositories();
        }


        public IGenericRepository<Position> positionRepository { get; private set; }
        public IGenericRepository<Factory> factoryRepository { get; private set; }
        public IGenericRepository<Unit> unitRepository { get; private set; }
        private void IniRepositories()
        {
            positionRepository = new GenericRepository<Position>(_context);
            factoryRepository = new GenericRepository<Factory>(_context);
            unitRepository = new GenericRepository<Unit>(_context);
        }

      
        public void save()
        {
            _context.SaveChanges();
        }
    }
    
}