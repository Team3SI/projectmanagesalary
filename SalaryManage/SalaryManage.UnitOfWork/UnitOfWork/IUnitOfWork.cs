﻿using SalaryManage.Repository;
using SalaryManage.Models;
namespace SalaryManage.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Position> positionRepository { get; }
        IGenericRepository<Factory> factoryRepository { get; }
        IGenericRepository<Unit> unitRepository { get; }
        void save();

    }
}