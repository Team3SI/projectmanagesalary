﻿using Microsoft.EntityFrameworkCore;

namespace SalaryManage.Models
{
    public class SalaryManageContext : DbContext
    {
        public SalaryManageContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Position> Positions { get; set; }
        public DbSet<Factory> Factories { get; set; }
        public DbSet<Unit> Units { get; set; }
    }
}