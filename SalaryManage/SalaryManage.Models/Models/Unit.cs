﻿using System.ComponentModel.DataAnnotations;

namespace SalaryManage.Models
{
    public class Unit
    {
        [Key]
        public string IdUnit { get; set; }
        public string NameUnit { get; set; }
    }
}