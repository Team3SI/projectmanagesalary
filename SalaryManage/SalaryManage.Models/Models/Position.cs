﻿using System.ComponentModel.DataAnnotations;

namespace SalaryManage.Models
{
    public class Position
    {
        [Key]
        public string IdPosition { get; set; }
        public string NamePosition { get; set; }
    }
}