﻿using System.ComponentModel.DataAnnotations;

namespace SalaryManage.Models
{
    public class Factory
    {
        [Key]
        public string IdFactory { get; set; }
        public string NameFactory { get; set; }
    }
}