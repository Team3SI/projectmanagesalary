﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using SalaryManage.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PositionController : ControllerBase
    {
      private readonly SalaryManageContext _context;

        public PositionController(SalaryManageContext context)
        {
           
            _context = context;

            if (_context.Positions.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Positions.Add(new Position{ IdPosition = "Item1"});
                _context.SaveChanges();
            }
        }
       
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Position>>> GetTodoItems() 
        {  
            return await _context.Positions.ToListAsync();
        }
       
        [HttpGet("{id}")]
        public async Task<ActionResult<Position>> GetPosition(string id)
        {
             var position = _context.Positions.Find(id);
                

            if (position == null)
            {
                return NotFound();
            }

            return position;
        }
//        
        // POST: api/values/5
        [HttpPost]
        public async Task<ActionResult<Position>> PostTodoItem(Position item)
        {
//           
            var positionName= _context.Positions.Where(s => s.NamePosition== item.NamePosition).FirstOrDefault<Position>();
            var positionId = _context.Positions.Find(item.IdPosition);
            
                if(positionName!=null|| positionId!=null)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest,("Reset Content Field name"));
                }
//
            try
            {
                _context.Positions.Add(item);
                await _context.SaveChangesAsync();
                return CreatedAtAction(nameof(GetPosition), new { id = item.IdPosition }, item);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest,e);
            }

           

        }

        // PUT api/values/5

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(string id, Position item)
        {
            
            try
            {
                if (id != item.IdPosition)
                {
                    return BadRequest();
                }

           
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();


        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(string id)
        {
         
            var position = await _context.Positions.FindAsync(id);

            if (position == null)
            {
                return NotFound();
            }

            try
            {
                _context.Positions.Remove(position);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
         

            
        }
    }

    
}