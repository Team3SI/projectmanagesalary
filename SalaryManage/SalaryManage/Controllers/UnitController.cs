﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using SalaryManage.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitController : ControllerBase
    {
      private readonly SalaryManageContext _context;

        public UnitController(SalaryManageContext context)
        {
           
            _context = context;

            if (_context.Units.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Units.Add(new Unit(){ IdUnit = "Item1"});
                _context.SaveChanges();
            }
        }
       
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Unit>>> GetTodoItems() 
        {  
            return await _context.Units.ToListAsync();
        }
       
        [HttpGet("{id}")]
        public async Task<ActionResult<Unit>> GetFactory(string id)
        {
             var unit = _context.Units.Find(id);
                

            if (unit == null)
            {
                return NotFound();
            }

            return unit;
        }
//        
        // POST: api/values/5
        [HttpPost]
        public async Task<ActionResult<Unit>> PostTodoItem(Unit item)
        {
//           
            var unitName= _context.Units.Where(s => s.NameUnit== item.NameUnit).FirstOrDefault<Unit>();
            var unitId = _context.Units.Find(item.IdUnit);
            
                if(unitName!=null|| unitId!=null)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest,("Reset Content Field name"));
                }
//
            try
            {
                _context.Units.Add(item);
                await _context.SaveChangesAsync();
                return CreatedAtAction(nameof(GetFactory), new { id = item.IdUnit }, item);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest,e);
            }

           

        }

        // PUT api/values/5

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(string id, Unit item)
        {
            
            try
            {
                if (id != item.IdUnit)
                {
                    return BadRequest();
                }

           
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();


        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(string id)
        {
         
            var unit = await _context.Units.FindAsync(id);

            if (unit == null)
            {
                return NotFound();
            }

            try
            {
                _context.Units.Remove(unit);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
         

            
        }
    }

    
}