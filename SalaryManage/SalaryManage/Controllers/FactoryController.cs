﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using SalaryManage.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FactoryController : ControllerBase
    {
      private readonly SalaryManageContext _context;

        public FactoryController(SalaryManageContext context)
        {
           
            _context = context;

            if (_context.Factories.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Factories.Add(new Factory(){ IdFactory = "Item1"});
                _context.SaveChanges();
            }
        }
       
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Factory>>> GetTodoItems() 
        {  
            return await _context.Factories.ToListAsync();
        }
       
        [HttpGet("{id}")]
        public async Task<ActionResult<Factory>> GetFactory(string id)
        {
             var factory = _context.Factories.Find(id);
                

            if (factory == null)
            {
                return NotFound();
            }

            return factory;
        }
//        
        // POST: api/values/5
        [HttpPost]
        public async Task<ActionResult<Factory>> PostTodoItem(Factory item)
        {
//           
            var factoryName= _context.Factories.Where(s => s.NameFactory== item.NameFactory).FirstOrDefault<Factory>();
            var factoryId = _context.Factories.Find(item.IdFactory);
            
                if(factoryName!=null|| factoryId!=null)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest,("Reset Content Field name"));
                }
//
            try
            {
                _context.Factories.Add(item);
                await _context.SaveChangesAsync();
                return CreatedAtAction(nameof(GetFactory), new { id = item.IdFactory }, item);
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest,e);
            }

           

        }

        // PUT api/values/5

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(string id, Factory item)
        {
            
            try
            {
                if (id != item.IdFactory)
                {
                    return BadRequest();
                }

           
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
                
            }
            catch (Exception)
            {
                throw;
            }

            return NoContent();


        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(string id)
        {
         
            var factory = await _context.Factories.FindAsync(id);

            if (factory == null)
            {
                return NotFound();
            }

            try
            {
                _context.Factories.Remove(factory);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
         

            
        }
    }

    
}