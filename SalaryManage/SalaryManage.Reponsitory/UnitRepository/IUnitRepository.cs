﻿using SalaryManage.Models;
using SalaryManage.Repository;

namespace SalaryManage.Reponsitory.UnitRepository
{
    public interface IUnitRepository : IGenericRepository<Unit>
    {
        
    }
}