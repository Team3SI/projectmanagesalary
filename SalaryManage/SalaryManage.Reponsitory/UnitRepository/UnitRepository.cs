﻿using SalaryManage.Models;
using SalaryManage.Repository;

namespace SalaryManage.Reponsitory.UnitRepository
{
    public class UnitRepository : GenericRepository<Unit>,IUnitRepository 
    {
        public UnitRepository(SalaryManageContext context) : base(context)
        {
        }
    }
}