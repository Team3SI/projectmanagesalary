﻿using Microsoft.Extensions.DependencyInjection;
using SalaryManage.Repository;
using SalaryManage.Models;
using SalaryManage.Reponsitory.FactoryRepository;
using SalaryManage.Reponsitory.UnitRepository;

namespace SalaryManage.Reponsitory
{
    public static class ReponsitoryCollection
    {
        public static IServiceCollection AddRepositoriesExtension(this IServiceCollection service)
        {
//            services.AddScoped<IPositionRepository,PositionRepository>();
//            services.AddScoped<IGenericRepository<Position>, GenericRepository<Position>>();
            service.AddScoped<IPositionRepository, PositionRepository>();
            service.AddScoped<IGenericRepository<Position>, GenericRepository<Position>>();
            service.AddScoped<IFactoryRepository, FactoryRepository.FactoryRepository>();
            service.AddScoped<IUnitRepository, UnitRepository.UnitRepository>();
            return service;
        }

    }
}