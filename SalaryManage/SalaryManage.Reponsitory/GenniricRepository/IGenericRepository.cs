﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SalaryManage.Repository
{
    public interface IGenericRepository<TEntity>
    {
        
        TEntity GetById(string Id);
        IEnumerable<TEntity> GetAll();
        Task Create(TEntity tentity);
        void Delete(string Id);
        Task Update(TEntity tentity);
        void save();
    }
}