﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalaryManage.Models;

namespace SalaryManage.Repository
{
    public class GenericRepository <TEntity> : IGenericRepository<TEntity> where  TEntity : class
    {
        private SalaryManageContext _context;
        private DbSet<TEntity> Dbset;
        public GenericRepository(SalaryManageContext context)
        {
            _context = context;
            this.Dbset = _context.Set<TEntity>();
            
        }

        public TEntity GetById(string Id)
        {
            return Dbset.Find(Id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return  Dbset;
        }
//
        public async Task Create(TEntity tentity)
        {
             
            Dbset.Add(tentity);
             
        }

        public void Delete(string Id)
        {
            var tentity = ((IGenericRepository<TEntity>) this).GetById(Id);
            if (tentity != null)
            {
                Dbset.Remove(tentity);
              
            }
     
        }

        public async Task Update(TEntity tentity)
        {

            Dbset.Update(tentity);
        }

        public void save()
        {
            _context.SaveChanges();
        }
        
    }
}


