﻿using SalaryManage.Models;

namespace SalaryManage.Repository
{
    public interface IPositionRepository : IGenericRepository<Position>
    {
        
    }
}