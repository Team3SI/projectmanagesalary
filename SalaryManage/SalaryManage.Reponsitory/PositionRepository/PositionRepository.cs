﻿using SalaryManage.Models;
namespace SalaryManage.Repository
{
    public class PositionRepository : GenericRepository<Position>,IPositionRepository
    {
        public  PositionRepository(SalaryManageContext context) : base(context)
        {
        }
    }
}