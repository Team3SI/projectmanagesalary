﻿using SalaryManage.Models;
using SalaryManage.Repository;

namespace SalaryManage.Reponsitory.FactoryRepository
{
    public interface IFactoryRepository : IGenericRepository<Factory>
    {
        
    }
}