﻿using SalaryManage.Models;
using SalaryManage.Repository;

namespace SalaryManage.Reponsitory.FactoryRepository
{
    public class FactoryRepository : GenericRepository<Factory>, IFactoryRepository
    {
        public FactoryRepository(SalaryManageContext context) : base(context)
        {
        }
    }
}